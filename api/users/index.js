'use strict'

var express = require('express')
var userController = require('./user.controller')

var router = express.Router()

// user
router.get('/users', userController.getAllUser)

// save user
router.post('/users', userController.addUser)


module.exports = router
