'use strict'

const Users = require('./user.models')

module.exports = {
    getAllUser: function(req, res){
      Users.find({})
      .exec((err, users) => {
        res.status(200).send({
          status: 200,
          message: users
        })
      })
    },

    addUser: function(req, res) {
        let params = req.body
        console.log('#### body ###', params)
        let user = new Users()

        user.name = params.name
        user.last_name = params.last_name
        user.age = params.age
        user.sexo = params.sexo
        user.city = params.city
        user.role = params.role


        user.save((err, userSave) => {
          if (err) {
            res.status(400).send({
              status: 400,
              message: 'Error create user'
            })
          }

          res.status(200).send({
            status: 200,
            message: 'User created',
            userSave: userSave
          })
        })
    }
}
