'user strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var userSchema = Schema({
  name: {
    type: String,
    required:[true, 'required']
  },
  last_name: {
    type: String,
    required: [true, 'required']
  },
  age: {
    type: String,
    required: [true, 'required']
  },
  sexo: {
    type: String,
    required: [true, 'required']
  },
  role: {
    type: String,
    required: [true, 'required']
  },
  city: {
    type: String,
    required: [true, 'required']
  }
})

module.exports = mongoose.model('Users', userSchema)
